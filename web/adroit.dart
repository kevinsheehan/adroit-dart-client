library adroit;

import 'dart:html' as html;
import 'dart:html' show KeyCode, WebSocket;
import 'dart:math' as math;
import 'dart:async';
import 'dart:convert';
import 'dart:collection';
import 'package:a_star/a_star.dart';
import 'package:stagexl/stagexl.dart';
import 'package:logging/logging.dart';

part 'src/assets/asset_handler.dart';

part 'src/data/defs.dart';

part 'src/messages/client_to_server.dart';
part 'src/messages/server_to_client.dart';

part 'src/model/entities.dart';
part 'src/model/location.dart';
part 'src/model/map.dart';
part 'src/model/pathing.dart';
part 'src/model/region.dart';

part 'src/screens/hud.dart';
part 'src/screens/screen.dart';

class Client {
  static final Logger logger = new Logger('Client');
  static const DEBUG = true;

  WebSocket webSocket;

  Stage stage;
  RenderLoop renderLoop;
  AssetHandler assetHandler;

  Screen currentScreen;

  Bitmap loadingBitmap;
  Tween loadingBitmapTween;
  TextField loadingTextField;

  Player me;

  Client() {
    assetHandler = new AssetHandler();
    stage = new Stage(html.querySelector('#stage'), webGL: true);
    renderLoop = new RenderLoop();
    renderLoop.addStage(stage);

    BitmapData.load('images/Loading.png').then((bitmapData) {
      loadingBitmap = new Bitmap(bitmapData);
      loadingBitmap.pivotX = 20;
      loadingBitmap.pivotY = 20;
      loadingBitmap.x = stage.stageWidth/2 - 10;
      loadingBitmap.y = stage.stageHeight/2 - 10;
      stage.addChild(loadingBitmap);

      loadingTextField = new TextField();
      loadingTextField.defaultTextFormat = new TextFormat('Helvetica,Arial', 20, 0xA0A0A0, bold:true);;
      loadingTextField.width = 240;
      loadingTextField.height = 40;
      loadingTextField.text = 'Loading ...';
      loadingTextField.x = 400 - loadingTextField.textWidth / 2;
      loadingTextField.y = 320;
      loadingTextField.mouseEnabled = false;
      stage.addChild(loadingTextField);

      loadingBitmapTween = new Tween(loadingBitmap, 100, TransitionFunction.linear);
      loadingBitmapTween.animate.rotation.to(100.0 * 2.0 * math.PI);
      stage.juggler.add(loadingBitmapTween);

      loadResources();
    });
  }

  void run() {
    stage.removeChild(loadingBitmap);
    stage.removeChild(loadingTextField);
    stage.juggler.remove(loadingBitmapTween);

    currentScreen = new LoginScreen(this, stage);
    stage.addChild(currentScreen);
    logger.info('Attempting to connect...');
    connect();
  }

  void connect() {
    webSocket = new WebSocket('ws://127.0.0.1:12345/ws');
    //webSocket = new WebSocket('ws://76.119.233.103:12345/ws');
    webSocket.onOpen.first.then((_) {
      logger.info('Connected to the server!');
      onConnected();
      webSocket.onClose.first.then((_) {
        logger.info('Connection disconnected to ${webSocket.url}');
        onDisconnected();
      });
      webSocket.onError.first.then((_) {
        logger.warning('Error');
      });
    });
  }

  void onConnected() {
    webSocket.onMessage.listen((event) {
      handleMessage(event.data);
    });
  }

  void onDisconnected() {
    // This will reload the Login Screen but it will not actually cause a reconnect
    //changeScreen(new LoginScreen(this, stage));

    // For now just draw a disconnected box on the screen
    var box = new BitmapData(400, 100, false, Color.Black);
    var boxBitmap = new Bitmap(box)
      ..x = stage.stageWidth/2 - 200
      ..y = stage.stageHeight/2 - 50;
    stage.addChild(boxBitmap);

    var font = new TextFormat('Helvetica,Arial', 20, Color.LawnGreen);
    var disconnectedText = new TextField('Disconnected from the Server', font)
      ..x = boxBitmap.x + 50
      ..y = boxBitmap.y + 40
      ..width = 300
      ..height = 20;
    stage.addChild(disconnectedText);
  }

  void handleMessage(data) {
    var json = new Map();
    try {
      json = JSON.decode(data);
      logger.finer(data);
      currentScreen.handleMessage(ServerMessage.getMessage(json));
    } on FormatException catch(e) {
      logger.warning('Malformed message from the server:\n' + data);
    } on UnimplementedError catch(e) {
      logger.warning('Unhandled message from server:\n' + data, e);
    }
  }

  void send(ClientMessage message) {
    webSocket.sendString(JSON.encode(message));
  }

  void changeScreen(Screen screen) {
    stage.removeChild(currentScreen);
    currentScreen.dispose();
    currentScreen = screen;
    stage.addChild(currentScreen);
  }

  void loadResources() {
    // todo: this works but it looks ugly, is there a better way?
    DefHandler.load().then((future) {
      assetHandler.loadDefTextures();
      assetHandler.resourceManager.load()
        .then((res) {
            run();
        })
        .catchError((error) {
          for(var resource in assetHandler.resourceManager.failedResources) {
            logger.severe('Loading resouce failed: ${resource.kind}.${resource.name} - ${resource.error}');
          }
          run();
        })
      .catchError((error) {
        logger.severe('Loading defintions failed.');
        run();
      });
    });
  }

  ResourceManager get resourceManager => assetHandler.resourceManager;
}

main() {
  html.querySelector('#header').text = 'Adroit MMORPG';
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((LogRecord rec) {
    print('${rec.level.name} [${rec.time}]: ${rec.loggerName} - ${rec.message}');
  });
  var client = new Client();
}
