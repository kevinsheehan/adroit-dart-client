part of adroit;

abstract class ServerMessage {
  String message;

  static ServerMessage getMessage(Map data) {
    switch (data['message']) {
      case 'login.response':
        return new LoginResponse.fromDataMap(data);
      case 'create.response':
        return new CreateResponse.fromDataMap(data);
      case 'chat.message':
        return new ChatMessage.fromDataMap(data);
      case 'add.object':
        return new AddObject.fromDataMap(data);
      case 'remove.object':
        return new RemoveObject.fromDataMap(data);
      case 'add.player':
        return new AddPlayer.fromDataMap(data);
      case 'update.player':
        return new UpdatePlayer.fromDataMap(data);
      case 'remove.player':
        return new RemovePlayer.fromDataMap(data);
      case 'add.npc':
        return new AddNpc.fromDataMap(data);
      case 'update.npc':
        return new UpdateNpc.fromDataMap(data);
      case 'remove.npc':
        return new RemoveNpc.fromDataMap(data);
      case 'send.inventory':
        return new SendInventory.fromDataMap(data);
      default:
        throw new UnimplementedError("Unknown opcode from server: '${data['message']}'");
    }
  }

  ServerMessage(String this.message);

  ServerMessage.fromDataMap(Map data);
}

class LoginResponse extends ServerMessage {
  int code;
  int region;
  int x;
  int y;

  LoginResponse.fromDataMap(Map data) : super(data['message']) {
    code = data['code'];
    region = data['region'];
    x = data['x'];
    y = data['y'];
  }
}

class CreateResponse extends ServerMessage {
  int code;

  CreateResponse.fromDataMap(Map data) : super(data['message']) {
    code = data['code'];
  }
}

class AddPlayer extends ServerMessage {
  String username;
  int x;
  int y;

  AddPlayer.fromDataMap(Map data) : super(data['message']) {
    username = data['username'];
    x = data['x'];
    y = data['y'];
  }
}

class UpdatePlayer extends ServerMessage {
  String username;
  int x;
  int y;

  UpdatePlayer.fromDataMap(Map data) : super(data['message']) {
    username = data['username'];
    x = data['x'];
    y = data['y'];
  }
}

class RemovePlayer extends ServerMessage {
  String username;

  RemovePlayer.fromDataMap(Map data) : super(data['message']) {
    username = data['username'];
  }
}

class AddNpc extends ServerMessage {
  int id;
  int defId;
  int x;
  int y;

  AddNpc.fromDataMap(Map data) : super(data['message']) {
    id = data['id'];
    defId = data['defId'];
    x = data['x'];
    y = data['y'];
  }
}

class UpdateNpc extends ServerMessage {
  int id;
  int x;
  int y;

  UpdateNpc.fromDataMap(Map data) : super(data['message']) {
    id = data['id'];
    x = data['x'];
    y = data['y'];
  }
}

class RemoveNpc extends ServerMessage {
  int id;

  RemoveNpc.fromDataMap(Map data) : super(data['message']) {
    id = data['id'];
  }
}

class ChatMessage extends ServerMessage {
  int type;
  String username;
  String text;

  ChatMessage.fromDataMap(Map data) : super(data['message']) {
    type = data['type'];
    username = data['username'];
    text = data['text'];
  }
}

class AddObject extends ServerMessage {
  int id;
  int dir;
  int x;
  int y;

  AddObject.fromDataMap(Map data) : super(data['message']) {
    id = data['id'];
    dir = data['dir'];
    x = data['x'];
    y = data['y'];
  }
}

class RemoveObject extends ServerMessage {
  int x;
  int y;

  RemoveObject.fromDataMap(Map data) : super(data['message']) {
    x = data['x'];
    y = data['y'];
  }
}

class SendInventory extends ServerMessage {
  List<int> inventory;
  List<int> quantities;

  SendInventory.fromDataMap(Map data) : super(data['message']) {
    inventory = new List<int>(30);
    quantities = new List<int>(30);
    int i = 0;
    for (int id in data['ids']) {
      inventory[i] = data['ids'][id];
      quantities[i++] = data['quantities'][id];
    }
  }
}