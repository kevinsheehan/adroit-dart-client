part of adroit;

abstract class ClientMessage {
  String message;

  ClientMessage(String this.message);

  Map toJson() {
    return { "message": message };
  }
}

class Login extends ClientMessage {
  String username;
  String password;

  Login(this.username, this.password) : super('login');

  Map toJson() {
    return { "message": message, "username": username, "password": password };
  }
}
class CreateAccount extends ClientMessage {
  String username;
  String password;

  CreateAccount(this.username, this.password) : super('create.account');

  Map toJson() {
    return { "message": message, "username": username, "password": password };
  }
}

class Logout extends ClientMessage { }
class AtObject extends ClientMessage { }
class Attack extends ClientMessage { }
class TalkTo extends ClientMessage { }

class WalkTo extends ClientMessage {
  int startX;
  int startY;
  List<int> waypointXoffsets;
  List<int> waypointYoffsets;

  WalkTo(this.startX, this.startY, this.waypointXoffsets, this.waypointYoffsets) : super('walk.to');

  WalkTo.fromPath(Path path) : super('walk.to') {
    startX = path.startX;
    startY = path.startY;
    waypointXoffsets = path.waypointXoffsets;
    waypointYoffsets = path.waypointYoffsets;
  }

  Map toJson() {
    return { "message": message, "startX": startX, "startY": startY,
        "waypointXoffsets": waypointXoffsets , "waypointYoffsets": waypointYoffsets };
  }
}

class Say extends ClientMessage {
  String text;

  Say(this.text) : super('say');

  Map toJson() {
    return { "message": message, "text": text };
  }
}