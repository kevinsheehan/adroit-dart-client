part of adroit;

abstract class Screen extends DisplayObjectContainer {
  static final Logger logger = new Logger('Screen');
  Client client;
  Stage stage;

  Screen(this.client, this.stage);

  void handleMessage(data);

  void dispose();
}

class LoginScreen extends Screen {
  static final Logger logger = new Logger('LoginScreen');
  TextField username;
  TextField password;
  TextField info;

  LoginScreen(Client client, Stage stage) : super(client, stage) {
    var background = client.resourceManager.getBitmapData('LoginScreen');
    addChild(new Bitmap(background));

    var labelTextFormat = new TextFormat('Helvetica,Arial', 14, Color.FloralWhite, bold:true);
    var infoTextFormat = new TextFormat('Helvetica,Arial', 14, Color.MediumVioletRed, bold:true);
    var inputTextFormat = new TextFormat('Helvetica,Arial', 14, Color.DarkSlateGray)
      ..leftMargin = 3
      ..rightMargin = 3;
    var buttonTextFormat = inputTextFormat.clone()
      ..align = TextFormatAlign.CENTER
      ..size = 12
      ..leftMargin = 0
      ..rightMargin = 0;

    info = new TextField('', infoTextFormat)
      ..x = stage.stageWidth/2 - 90
      ..y = stage.stageHeight/2 + 22
      ..autoSize = TextFieldAutoSize.CENTER
      ..width = 200
      ..height = 20;
    addChild(info);

    addUsernameFields(labelTextFormat, inputTextFormat);
    addPasswordFields(labelTextFormat, inputTextFormat);
    addButtons(buttonTextFormat);

    stage.focus = username;
  }

  void addUsernameFields(TextFormat labelTextFormat, TextFormat inputTextFormat) {
    var usernameLabel = new TextField('Username: ', labelTextFormat)
      ..x = stage.stageWidth/2 - 90
      ..y = stage.stageHeight/2 - 20
      ..width = 80
      ..height = 20;
    addChild(usernameLabel);

    username = new TextField()
      ..defaultTextFormat = inputTextFormat
      ..background = true
      ..backgroundColor = Color.FloralWhite
      ..type = TextFieldType.INPUT
      ..x = stage.stageWidth/2 - 10
      ..y = stage.stageHeight/2 - 20
      ..width = 120
      ..height = 20
      ..onKeyDown.listen((KeyboardEvent keyboardEvent) {
        if (keyboardEvent.keyCode == html.KeyCode.TAB || keyboardEvent.keyCode == html.KeyCode.ENTER) {
          stage.focus = password;
          keyboardEvent.stopPropagation();
        }
      });
    username.onMouseClick.listen((mouseEvent) {
      stage.focus = username;
    });
    addChild(username);
  }

  void addPasswordFields(TextFormat labelTextFormat, TextFormat inputTextFormat) {
    var passwordLabel = new TextField('Password: ', labelTextFormat)
      ..x = stage.stageWidth/2 - 90
      ..y = stage.stageHeight/2
      ..width = 80
      ..height = 20;
    addChild(passwordLabel);

    password = new TextField()
      ..defaultTextFormat = inputTextFormat
      ..background = true
      ..backgroundColor = Color.FloralWhite
      ..displayAsPassword = true
      ..type = TextFieldType.INPUT
      ..x = stage.stageWidth/2 - 10
      ..y = stage.stageHeight/2
      ..width = 120
      ..height = 20
      ..onKeyDown.listen((KeyboardEvent keyboardEvent) {
        if (keyboardEvent.keyCode == KeyCode.TAB) {
          stage.focus = username;
          keyboardEvent.stopPropagation();
        } else if (keyboardEvent.keyCode == KeyCode.ENTER) {
          login();
          keyboardEvent.stopPropagation();
        }
      });
    password.onMouseClick.listen((mouseEvent) {
      stage.focus = password;
    });
    addChild(password);
  }

  void addButtons(TextFormat textFormat) {
    var createUp = new Bitmap(new BitmapData(90, 20, false, Color.FloralWhite));
    var createOver = new Bitmap(new BitmapData(90, 20, false, Color.LightSlateGray));
    var createDown = new Bitmap(new BitmapData(90, 20, false, Color.SlateGray));
    var createButton = new SimpleButton(createUp, createOver, createDown, createUp)
      ..x = stage.stageWidth/2 - 90
      ..y = stage.stageHeight/2 + 45
      ..onMouseClick.listen((e) {
        createAccount();
      });
    addChild(createButton);
    var createLabel = new TextField('Create Account', textFormat)
      ..mouseEnabled = false
      ..x = stage.stageWidth/2 - 90
      ..y = stage.stageHeight/2 + 47
      ..width = 90
      ..height = 20;
    addChild(createLabel);

    var loginUp = new Bitmap(new BitmapData(90, 20, false, Color.FloralWhite));
    var loginOver = new Bitmap(new BitmapData(90, 20, false, Color.LightSlateGray));
    var loginDown = new Bitmap(new BitmapData(90, 20, false, Color.SlateGray));
    var loginButton = new SimpleButton(loginUp, loginOver, loginDown, loginUp)
      ..x = stage.stageWidth/2 + 20
      ..y = stage.stageHeight/2 + 45
      ..onMouseClick.listen((mouseEvent) {
        login();
      });
    addChild(loginButton);
    var loginLabel = new TextField('Login', textFormat)
      ..mouseEnabled = false
      ..x = stage.stageWidth/2 + 20
      ..y = stage.stageHeight/2 + 47
      ..width = 90
      ..height = 20;
    addChild(loginLabel);
  }

  void createAccount() {
    if (username.text != '' && password.text != '') {
      var create = new CreateAccount(username.text, password.text);
      client.send(create);
    }
  }

  void login() {
    if (username.text != '' && password.text != '') {
      var login = new Login(username.text, password.text);
      client.send(login);
    }
  }

  void handleMessage(ServerMessage message) {
    if (message is LoginResponse)
      handleLoginResponse(message);
    else if (message is CreateResponse)
      handleCreateResponse(message);
    else
      logger.warning("Unhandled message: '${message.message}'");
  }

  void handleLoginResponse(LoginResponse response) {
    switch (response.code) {
      case 1:
        logger.info("Successfully logged in.");
        var loc = new Location.fromPosition(response.region, response.x, response.y);
        EntityDef def = new EntityDef()
          ..name = username.text
          ..texture = 'player';
        client.me = new Player(def, null, loc, client.resourceManager.getBitmapData('player'));
        var gameScreen = new GameScreen(loc, client, stage);
        client.changeScreen(gameScreen);
        break;
      case 10:
        info.text = 'Incorrect username or password.';
        logger.info("Incorrect username or password.");
        break;
      default:
        logger.warning("Unknown login code received from ConnectionHandler.");
        break;
    }
  }

  void handleCreateResponse(CreateResponse response) {
    switch (response.code) {
      case 0:
        info.text = 'Account name already taken.';
        logger.info("Account name already taken.");
      break;
      case 1:
        info.text = 'Successfully created account.';
        logger.info("Successfully created account.");
      break;
      default:
        logger.warning("Unknown login code received from ConnectionHandler.");
        break;
    }
  }

  void dispose() { }
}

class GameScreen extends Screen {
  static final Logger logger = new Logger('GameScreen');
  Region region;
  Hud hud;
  int lastUpdate = new DateTime.now().millisecondsSinceEpoch;
  int tick = 300;

  GameScreen(Location loc, client, stage) : super(client, stage) {
    this.region = new Region.fromId(this, client, loc.region);
    hud = new Hud(this);

    addChild(region);
    addChild(hud);
  }

  void render(RenderState renderState) {
    int currentTime = new DateTime.now().millisecondsSinceEpoch;
    if (currentTime - lastUpdate > tick) {
      lastUpdate = currentTime;
      client.me.update(renderState.deltaTime);
      region.update(renderState.deltaTime);
    }

    super.render(renderState);
  }

  void changeRegion(int region) {
    var tempRegion = new Region.fromId(this, client, region);
    if (this.region != null) {
      this.region.cleanup();
      removeChild(this.region);
    }
    this.region = tempRegion;
    addChild(this.region);
  }

  void handleMessage(ServerMessage message) {
    if (message is SendInventory)
      handleSendInventory(message);
    else if (message is AddObject)
      handleAddObject(message);
    else if (message is RemoveObject)
      handleRemoveObject(message);
    else if (message is AddPlayer)
      handleAddPlayer(message);
    else if (message is UpdatePlayer)
      handleUpdatePlayer(message);
    else if (message is RemovePlayer)
      handleRemovePlayer(message);
    else if (message is AddNpc)
      handleAddNpc(message);
    else if (message is UpdateNpc)
      handleUpdateNpc(message);
    else if (message is RemoveNpc)
      handleRemoveNpc(message);
    else if (message is ChatMessage)
      handleChatMessage(message);
    else
      logger.warning("Unhandled message: '${message.message}'");
  }

  void handleSendInventory(SendInventory inventory) {
    client.me.inventory = inventory.inventory;
  }

  void handleAddObject(AddObject object) {
    GameObjDef def = DefHandler.gameObjDefs[object.id];
    Location loc = new Location.fromPosition(region.regionId, object.x, object.y);
    GroundObject groundObject = new GroundObject(def, region, loc, client.resourceManager.getBitmapData(def.texture));
    region.addEntity(groundObject);
  }

  void handleRemoveObject(RemoveObject object) {
    GroundObject groundObject = region.map.getTile(object.x, object.y).object;
    region.removeEntity(groundObject);
  }

  void handleAddPlayer(AddPlayer addPlayer) {
    EntityDef def = new EntityDef()
      ..name = addPlayer.username
      ..texture = 'player';
    var player = new Player(def, region, new Location.fromPosition(region.regionId, addPlayer.x, addPlayer.y),
        client.resourceManager.getBitmapData('player'));
    region.addEntity(player);
  }

  void handleUpdatePlayer(UpdatePlayer updatePlayer) {
    var player = updatePlayer.username == client.me.name ? client.me : region.players[updatePlayer.username];
    Tile end = region.map.walkableGraph[updatePlayer.x][updatePlayer.y];
    region.pathFinder.findPath(region.map.walkableGraph[player.loc.absX][player.loc.absY], end).then((Queue<Tile> tiles) {
      List<int> xOffsets = new List<int>();
      List<int> yOffsets = new List<int>();
      for (var tile in tiles) {
        xOffsets.add(tile.loc.absX - player.loc.absX);
        yOffsets.add(tile.loc.absY - player.loc.absY);
      }
      player.path = new Path(player.loc.absX, player.loc.absY, xOffsets, yOffsets);
    });
  }

  void handleRemovePlayer(RemovePlayer removePlayer) {
    var player = region.players[removePlayer.username];
    if (player != null)
      region.removeEntity(player);
  }

  void handleAddNpc(AddNpc addNpc) {
    NpcDef def = DefHandler.npcDefs[addNpc.defId];
    var npc = new Mob(def, region, new Location.fromPosition(region.regionId, addNpc.x, addNpc.y),
        client.resourceManager.getBitmapData(def.texture));
    npc.id = addNpc.id;
    region.addEntity(npc);
  }

  void handleUpdateNpc(UpdateNpc updateNpc) {
    var npc = region.mobs[updateNpc.id];
    Tile end = region.map.walkableGraph[updateNpc.x][updateNpc.y];
    region.pathFinder.findPath(region.map.walkableGraph[npc.loc.absX][npc.loc.absY], end).then((Queue<Tile> tiles) {
      List<int> xOffsets = new List<int>();
      List<int> yOffsets = new List<int>();
      for (var tile in tiles) {
        xOffsets.add(tile.loc.absX - npc.loc.absX);
        yOffsets.add(tile.loc.absY - npc.loc.absY);
      }
      npc.path = new Path(npc.loc.absX, npc.loc.absY, xOffsets, yOffsets);
    });
  }

  void handleRemoveNpc(RemoveNpc removeNpc) {
    var npc = region.mobs[removeNpc.id];
    if (npc != null)
      region.removeEntity(npc);
  }

  void handleChatMessage(ChatMessage chatMessage) {
    //chatMessages.insert(chatMessage);
    if (chatMessage.type == 0) {
      var player = chatMessage.username == client.me.def.name ? client.me : region.players[chatMessage.username];
      if (player != null) {
        logger.fine('${chatMessage.username}: ${chatMessage.text}');
        //hud.addPlayerChat(player, chatMessage.text);
        hud.addChatMessage(chatMessage.type, '${chatMessage.username}: ${chatMessage.text}');
      }
    } else {
      hud.addChatMessage(chatMessage.type, chatMessage.text);
    }
  }

  void dispose() { }
}