part of adroit;

class Hud extends Sprite {
  static final Logger logger = new Logger('Hud');
  GameScreen screen;

  /// Tabs should be ordered as they will be opened by index
  List<ActionTab> actionTabs;

  static final TextFormat standardText = new TextFormat('Helvetica,Arial', 14, Color.AntiqueWhite);
  static final TextFormat standardChat = new TextFormat('Helvetica,Arial', 14, Color.Yellow);
  static final TextFormat expText = new TextFormat('Helvetica,Arial', 14, Color.SpringGreen);
  static final TextFormat announcementText = new TextFormat('Helvetica,Arial', 14, Color.Fuchsia);
  static final TextFormat alertText = new TextFormat('Helvetica,Arial', 14, Color.DarkRed);

  TextField mouseOver = new TextField();
  TextField chatBox = new TextField();

  TextField username;
  TextFormat userChatFont = standardChat.clone();
  TextField _userChat = new TextField();
  bool random = false;

  TextField fps;
  num frameRate = null;

  List<TextField> messageBox = new List<TextField>();

  Hud(this.screen) {
    createMouseOver();
    createChatBox();

    if (Client.DEBUG) {
      createFPS();
    }
  }

  void createMouseOver() {
    mouseOver = new TextField('This text should be temporary', standardText)
      ..mouseEnabled = false
      ..x = 10
      ..y = 10
      ..width = 700
      ..height = 20;

    addChild(mouseOver);
  }

  void createChatBox() {
    username = new TextField(screen.client.me.def.name + ': ', standardChat)
      ..mouseEnabled = false
      ..x = 10
      ..y = screen.stage.stageHeight - 25
      ..height = 20
      ..width = 150;
    logger.fine('Username field: ${username.x}, ${username.y}');
    addChild(username);

    _userChat = new TextField()
      ..mouseEnabled = false
      ..defaultTextFormat = userChatFont
      ..type = TextFieldType.INPUT
      ..x = username.x + username.textWidth
      ..y = username.y
      ..maxChars = 140
      ..height = 20
      ..width = screen.stage.width
      ..onKeyDown.listen((KeyboardEvent keyboardEvent) {
        if (keyboardEvent.keyCode == html.KeyCode.ENTER) {
          //if ((chatHasColor(_userChat.text) && _userChat.text.length > 4) || (!chatHasColor(_userChat.text) && _userChat.text.length > 0)) {
          if (_userChat.text.length > 0) {
            var say = new Say(_userChat.text);
            screen.client.send(say);
            resetUserChat();
          }
          keyboardEvent.stopPropagation();
        }
      });
    screen.stage.focus = _userChat;
    addChild(_userChat);
  }

  void resetUserChat() {
    random = false;
    userChatFont.color = Color.Yellow;
    _userChat.text = '';
  }

  void createFPS() {
    this.onEnterFrame.listen(_onEnterFrame);
    var debugFont = standardText.clone()
      ..color = Color.LawnGreen;
    fps = new TextField('FPS: ', debugFont)
      ..mouseEnabled = false
      ..x = screen.stage.stageWidth - 80
      ..y = screen.stage.stageHeight - (10 + debugFont.size)
      ..width = 52
      ..height = 20;

    addChild(fps);
  }

  void render(RenderState renderState) {
    if (Client.DEBUG && fps != null) {
      fps.text = 'FPS: ${frameRate}';
    }
    super.render(renderState);
  }

  _onEnterFrame(EnterFrameEvent e) {
    if (frameRate == null) {
      frameRate = 1.00 / e.passedTime;
    } else {
      frameRate = 0.05 / e.passedTime + 0.95 * frameRate;
    }
  }

  void addChatMessage(int type, String message) {
    for (var chatMessage in messageBox) {
      chatMessage.y -= 14;
    }
    if (messageBox.length == 5) {
      removeChild(messageBox.removeLast());
    }
    var chatMessage = new TextField(message, getTextFormat(type))
      ..mouseEnabled = false
      ..x = username.x
      ..y = username.y - 17
      ..height = username.height
      ..width = screen.stage.width;

    messageBox.insert(0, chatMessage);
    addChild(chatMessage);
  }

  TextFormat getTextFormat(int type) {
    switch (type) {
      case 0:
        return standardChat;
      case 10:
        return standardText;
      case 11:
        return expText;
      case 20:
        return announcementText;
      case 21:
        return alertText;
    }
    return standardText;
  }
}

abstract class ActionTab {
}
