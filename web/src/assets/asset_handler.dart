part of adroit;

class AssetHandler {
  static final Logger logger = new Logger('AssetHandler');
  final ResourceManager resourceManager = new ResourceManager();

  AssetHandler() {
    addRegions();
    addImages();
    addSounds();
    addAudio();
  }

  void addRegions() {
    resourceManager.addBitmapData('tutorial', 'images/map/tutorial_32.png');
  }

  void loadDefTextures() {
    logger.info('Texture def length: ${DefHandler.gameObjDefs.length}');
    DefHandler.gameObjDefs.values.forEach((def) => resourceManager.addBitmapData(def.texture, 'images\\${def.texture}'));
    DefHandler.itemDefs.values.forEach((def) => resourceManager.addBitmapData(def.texture, 'images\\${def.texture}'));
    DefHandler.npcDefs.values.forEach((def) => resourceManager.addBitmapData(def.texture, 'images\\${def.texture}'));

    resourceManager.addBitmapData('player', 'images/npcs/player.png');

    /*resourceManager.addBitmapData('images/objs/trees/tree.png', 'images/objs/trees/tree.png');
    resourceManager.addBitmapData('images/objs/trees/treetrunk.png', 'images/objs/trees/treetrunk.png');*/
  }

  void addImages() {
    resourceManager.addBitmapData('LoginScreen', 'images/screens/login.png');
  }

  void addSounds() { }

  void addAudio() { }
}
