part of adroit;

class Tile extends Object with Node {
  Location loc;

  List<Player> players = new List<Player>();
  List<Mob> mobs = new List<Mob>();
  List<GroundItem> items = new List<GroundItem>();
  GroundObject object = null;

  bool walkable = true;

  Tile(this.loc) { }

  String get topAction {
    if (items.length > 0) {
      return 'Pick-up ${items[0].def.name}';
    }
    if (object != null && (object.def as GameObjDef).action1 != null) {
      return (object.def as GameObjDef).action1 + ' ' + (object.def as GameObjDef).name;
    }
    if (mobs.length > 0) {
      NpcDef def = mobs[0].def as NpcDef;
      String action = def.action;
      if (action == null) {
        if (def.attackable)
          action = "Attack";
        else
          action = "Walk-to";
      }
      return '$action ${def.name} [Level-${def.level}]';
    }
    if (players.length > 0) {
      EntityDef def = players[0].def;
      return 'Walk-to ${def.name}';
    }
    return '';
  }

  void add(Entity e) {
    if (e is Player) {
      players.add(e);
    } else if (e is Mob) {
      mobs.add(e);
    } else if (e is GroundItem) {
      items.add(e);
    }
  }

  void remove(Entity e) {
    if (e is Player) {
      players.remove(e);
    } else if (e is Mob) {
      mobs.remove(e);
    } else if (e is GroundItem) {
      items.remove(e);
    }
  }

  String toString() {
    return 'Tile {location: $loc, walkable: $walkable, object: $object}';
  }

  /// Two tiles are the same if they share the same coordinate, there shouldn't be more than 1 tile per coordinate ever
  bool operator ==(other) {
    return other is Tile && other.loc.absX == loc.absX && other.loc.absY == loc.absY;
  }
}

class TileMap extends Bitmap implements Graph<Tile> {
  List<List<Tile>> walkableGraph = new List<List<Tile>>();
  List<Tile> tiles = new List<Tile>();

  Region region;

  TileMap(this.region, BitmapData bitmapData) : super(bitmapData) {
    generateTileMap();
  }

  /// Take the bitmap data for this object and generate the walkable Tile Map from it
  void generateTileMap() {
    var unwalkable = [ "0x606260", "0x1f3f7d" ];
    int mapWidth = (width/Region.SIZE).floor();
    int mapHeight = (height/Region.SIZE).floor();
    for (int x = 0; x < mapWidth; x++) {
      walkableGraph.add(new List<Tile>(mapHeight));
      for (int y = 0; y < mapHeight; y++) {
        Tile tile = new Tile(new Location.fromPosition(region.regionId, x, y));
        tiles.add(tile);

        int pixel = bitmapData.getPixel(x*Region.SIZE, height.floor()-y*Region.SIZE);
        for (var hexPixel in unwalkable) {
          if (pixel == int.parse(hexPixel)) {
            tile.walkable = false;
            break;
          }
        }
        if (tile.walkable) walkableGraph[x][y] = tile;
        /* // This will be used later for mapping tiles and their textures
        if (!AssetHandler.tileSet.containsKey(color)) {
          Byte b = count;
          AssetHandler.tileSet.put(color, b);
          AssetHandler.tileArt.put(b, color);
          count++;
        }
        tiles[x][y] = new Tile(AssetHandler.tileSet.get(color), Point.location(x, y));
        if (color.equals(stoneWall) || color.equals(water)) {
          tiles[x][y].setWalkable(false);
        }*/
      }
    }
  }

  Tile getTile(int x, int y) => tiles[x*(width/Region.SIZE).floor() + y];

  bool isWalkable(int x, int y) => walkableGraph[x][y] != null && (walkableGraph[x][y].object == null || (walkableGraph[x][y].object.def as GameObjDef).walkable);

  Iterable<Tile> get allNodes => tiles;

  num getDistance(Tile a, Tile b) => math.sqrt((a.loc.absX-b.loc.absX).abs() + (a.loc.absY-b.loc.absY).abs());

  num getHeuristicDistance(Tile a, Tile b) => math.sqrt((a.loc.absX-b.loc.absX).abs() + (a.loc.absY-b.loc.absY).abs());

  List<List<int>> xyOffsets = [[-1, -1], [1, -1], [1, 1], [-1, 1], [0, -1], [-1, 0], [0, 1], [1, 0]];
  Iterable<Tile> getNeighboursOf(Tile node) {
    var x = node.loc.absX;
    var y = node.loc.absY;
    List<Tile> neighbors = new List<Tile>();
    for (var xyOffset in xyOffsets) {
      //if (walkableGraph[x+xyOffset[0]][y+xyOffset[1]] != null) {
      if (isWalkable(x+xyOffset[0], y+xyOffset[1])) {
        // Handle diagonal cases
        // All tiles need to be reachable:
        // player = p, x = goal, r = reachable tiles
        // p r
        // r x
        if (xyOffset[0].abs() == 1 && xyOffset[1].abs() == 1) {
          //if (walkableGraph[x+xyOffset[0]][y] == null || walkableGraph[x][y+xyOffset[1]] == null)
          if (!isWalkable(x+xyOffset[0], y) || !isWalkable(x, y+xyOffset[1]))
            continue;
        }
        neighbors.add(walkableGraph[x+xyOffset[0]][y+xyOffset[1]]);
      }
    }
    return neighbors;
  }
}
