part of adroit;

/// Entity is basically a Bitmap with a relative Game location
class Entity extends Bitmap {
  static final Logger logger = new Logger('Entity');
  Location loc;
  Region region;

  EntityDef def;

  int id;

  var tween;

  num get screenX => loc.absX*Region.SIZE;
  num get screenY => region.height - loc.absY*Region.SIZE;

  Entity(this.def, this.region, this.loc, [BitmapData bitmapData]) : super(bitmapData) { }

  void render(RenderState renderState) {
    super.render(renderState);
  }

  /// Position is for movement within a region
  set position(Point p) {
    if (tween != null) {
      tween.complete();
    }

    tween = new Tween(this, 0.3, TransitionFunction.linear);
    tween.animate.x.by(Region.SIZE*(p.x-this.loc.absX));
    tween.animate.y.by(Region.SIZE*(this.loc.absY-p.y));
    tween.onComplete = () {
      logger.finest('Step completed to (${p.x}, ${p.y})');
      stage.juggler.remove(tween);
      if (this != region.client.me)
        region.setPosition(this, this.loc, p);
      loc.absX = p.x;
      loc.absY = p.y;
      tween = null;
    };
    stage.juggler.add(tween);
    x = screenX;
    y = screenY;
  }

  /// Use set location when the entity has changed regions
  set location(Location loc) {
    if (loc.region == this.loc.region) {
      this.loc = loc;
      x = screenX;
      y = screenY;
    } else {
      position = new Point(loc.absX, loc.absY);
    }
  }

  String toString() {
    return def.name;
  }
}

class GroundObject extends Entity {
  GroundObject(GameObjDef def, Region region, Location loc, [BitmapData bitmapData]) : super(def, region, loc, bitmapData) { }
}

class GroundItem extends Entity {
  GroundItem(ItemDef def, Region region, Location loc, [BitmapData bitmapData]) : super(def, region, loc, bitmapData) { }
}

class Mob extends Entity {
  bool attackable = false;
  PathHandler pathHandler;

  Mob(EntityDef def, Region region, Location loc, [BitmapData bitmapData]) : super(def, region, loc, bitmapData) {
    pathHandler = new PathHandler(this);
  }

  void update(num delta) {
    pathHandler.updatePosition();
  }

  set path(Path path) {
    pathHandler.path = path;
  }
}

class Player extends Mob {
  static final Logger logger = new Logger('Player');
  List<int> inventory;

  Player(EntityDef def, Region region, Location loc, [BitmapData bitmapData]) : super(def, region, loc, bitmapData) { }
}
