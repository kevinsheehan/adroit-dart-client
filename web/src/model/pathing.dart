part of adroit;

class Path {
  bool noClip = false;
  int startX, startY;
  List<int> waypointXoffsets, waypointYoffsets;

  Path(this.startX, this.startY, this.waypointXoffsets, this.waypointYoffsets) { }

  Path.fromSingleMove(int x, int y, int endX, int endY, [this.noClip]) {
    startX = endX;
    startY = endY;
    waypointXoffsets = new List<int>(0);
    waypointYoffsets = new List<int>(0);
  }

  int getWaypointX(int wayPoint) {
    return startX + waypointXoffsets[wayPoint];
  }

  int getWaypointY(int wayPoint) {
    return startY + waypointYoffsets[wayPoint];
  }

  int get length => waypointXoffsets == null ? 0 : waypointXoffsets.length;

  String toString() {
    return "Start X: $startX\nStart Y: $startY\nXOffsets: $waypointXoffsets\nYOffsets: $waypointYoffsets";
  }
}

class PathHandler {
  static final Logger logger = new Logger('PathHandler');
  int curWaypoint;
  Mob me;
  Path _path;

  /**
   * Resets the path (stops movement)
   */
  void resetPath() {
    path = null;
  }

  PathHandler(Mob m) {
    me = m;
    resetPath();
  }

  bool get atStart => me.loc.absX == _path.startX && me.loc.absY == _path.startY;

  bool atWaypoint(int waypoint) {
    return _path.getWaypointX(waypoint) == me.loc.absX && _path.getWaypointY(waypoint) == me.loc.absY;
  }

  /// Stops us in our tracks
  List<int> cancelPath() {
    resetPath();
    return [ -1, -1 ];
  }

  bool get finishedPath => _path == null ? true : (_path.length > 0 ? atWaypoint(_path.length - 1) : atStart);

  bool isWalkable(int x, int y) {
    return me.region.map.isWalkable(x, y);
  }

  /**
   * Gets the next coordinate in the right direction
   */
  List<int> getNextCoords(int startX, int destX, int startY, int destY) {
    try {
      List<int> coords = [ startX, startY ];
      bool myXBlocked = false, myYBlocked = false, newXBlocked = false, newYBlocked = false;
      if (startX > destX) {
        myXBlocked = !isWalkable(startX - 1, startY); // Check right
        // tiles left
        // wall
        coords[0] = startX - 1;
      } else if (startX < destX) {
        myXBlocked = !isWalkable(startX + 1, startY); // Check left
        // tiles right
        // wall
        coords[0] = startX + 1;
      }

      if (startY > destY) {
        myYBlocked = !isWalkable(startX, startY - 1); // Check top
        // tiles bottom
        // wall
        coords[1] = startY - 1;
      } else if (startY < destY) {
        myYBlocked = !isWalkable(startX, startY + 1); // Check bottom
        // tiles top
        // wall
        coords[1] = startY + 1;
      }

      // If both directions are blocked OR we are going straight and the
      // direction is blocked
      if ((myXBlocked && myYBlocked) || (myXBlocked && startY == destY) || (myYBlocked && startX == destX)) {
        return cancelPath();
      }

      if (coords[0] > startX) {
        newXBlocked = !isWalkable(coords[0], coords[1]);
        // Check dest
        // tiles
        // right
        // wall
      } else if (coords[0] < startX) {
        newXBlocked = !isWalkable(coords[0], coords[1]);
        // Check dest
        // tiles
        // left wall
      }

      if (coords[1] > startY) {
        newYBlocked = !isWalkable(coords[0], coords[1]);
        // Check dest
        // tiles top
        // wall
      } else if (coords[1] < startY) {
        newYBlocked = !isWalkable(coords[0], coords[1]);
        // Check dest
        // tiles
        // bottom
        // wall
      }

      // If both directions are blocked OR we are going straight and the
      // direction is blocked
      if ((newXBlocked && newYBlocked) || (newXBlocked && startY == coords[1]) || (myYBlocked && startX == coords[0])) {
        return cancelPath();
      }

      // If only one direction is blocked, but it blocks both tiles
      if ((myXBlocked && newXBlocked) || (myYBlocked && newYBlocked)) {
        return cancelPath();
      }

      return coords;
    } catch (e) {
      return cancelPath();
    }
  }

  /**
   * Updates our position to the next in the path
   */
  void setNextPosition() {
    List<int> newCoords = [ -1, -1 ];
    if (curWaypoint == -1) {
      if (atStart) {
        curWaypoint = 0;
      } else {
        newCoords = getNextCoords(me.loc.absX, _path.startX, me.loc.absY, _path.startY);
      }
    }
    if (curWaypoint > -1) {
      if (atWaypoint(curWaypoint)) {
        curWaypoint++;
      }
      if (curWaypoint < _path.length) {
        newCoords = getNextCoords(me.loc.absX, _path.getWaypointX(curWaypoint), me.loc.absY, _path.getWaypointY(curWaypoint));
      } else {
        resetPath();
      }
    }
    if (newCoords[0] > -1 && newCoords[1] > -1) {
      me.position = new Point(newCoords[0], newCoords[1]);
    }
  }

  void setPath(int startX, int startY, List<int> waypointXoffsets, List<int> waypointYoffsets) {
    path = new Path(startX, startY, waypointXoffsets, waypointYoffsets);
  }

  set path(Path path) {
    curWaypoint = -1;
    _path = path;
  }

  void updatePosition() {
    if (!finishedPath) {
      setNextPosition();
    }
  }
}
