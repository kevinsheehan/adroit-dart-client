part of adroit;

class Region extends Sprite {
  static final Logger logger = new Logger('Region');
  Client client;
  GameScreen screen;
  int regionId;

  TileMap map;
  AStar pathFinder;

  Sprite objectLayer;
  Sprite itemLayer;
  Sprite playerLayer;
  Sprite mobLayer;

  List<GroundObject> groundObjects = new List<GroundObject>();
  List<GroundItem> items = new List<GroundItem>();
  Map<String, Player> players = new HashMap();
  Map<int, Mob> mobs = new HashMap();

  static const int SIZE = 32;

  num get screenX => -((me.loc.absX*SIZE) - stage.stageWidth/2);
  num get screenY => -(map.width - (me.loc.absY*SIZE)+SIZE/2 - stage.stageHeight/2);

  Player get me => client.me;

  num get width => map.width;
  num get height => map.height;

  num get localWidth => map.width/SIZE;
  num get localHeight => map.height/SIZE;

  num get localX => (mouseX/SIZE).floor();
  num get localY => ((height - mouseY)/SIZE).ceil();

  Region.fromId(this.screen, this.client, this.regionId) {
    map = new TileMap(this, client.resourceManager.getBitmapData("tutorial"));
    pathFinder = new AStar(map);

    groundObjects = new List<GroundObject>();
    items = new List<GroundItem>();

    objectLayer = new Sprite();
    itemLayer = new Sprite();
    playerLayer = new Sprite();
    mobLayer = new Sprite();
    addChild(map);
    addChild(objectLayer);
    addChild(itemLayer);
    addChild(playerLayer);
    addChild(mobLayer);
    // Player is always on top of everything (except Hud)
    addChild(me);
    me.region = this;
    me.location = me.loc;

    addEventListener(MouseEvent.MOUSE_OVER, mapMouseOver);
    addEventListener(MouseEvent.MOUSE_DOWN, mapTouchDown);
  }

  void mapMouseOver(MouseEvent event) {
    screen.hud.mouseOver.text = 'Mouse Position: Local:(${event.localX}, ${event.localY}), Stage:(${event.stageX}, ${event.stageY}), ' +
        'Delta:(${event.deltaX}, ${event.deltaY})';
  }

  void mapTouchDown(MouseEvent event) {
    Point point = screenToPoint(event.localX, event.localY);
    logger.finer("we pressed on the map at: ${point.x}, ${point.y}");
    Tile end = map.walkableGraph[point.x][point.y];
    if (end == null) {
      return;
    }
    // We want to walk somewhere!
    // todo: renderer.addClick(Point.location(screenX, height-screenY), Renderer.ClickType.Walk);

    // todo: modify the algorithm so that it can have an adjacent stop
    pathFinder.findPath(map.walkableGraph[me.loc.absX][me.loc.absY], end).then((Queue<Tile> tiles) {
      if (tiles.length < 2) return;
      List<int> xOffsets = new List<int>();
      List<int> yOffsets = new List<int>();
      int xDir = tiles.elementAt(1).loc.absX - tiles.elementAt(0).loc.absX;
      int yDir = tiles.elementAt(1).loc.absY - tiles.elementAt(0).loc.absY;
      var txDir;
      var tyDir;
      for (int offset = 1; true; offset++) {
        var curLoc = tiles.elementAt(offset).loc;
        logger.finest('Cur: $curLoc');
        if (offset > 1) {
          var prevLoc = tiles.elementAt(offset - 1).loc;
          txDir = curLoc.absX - prevLoc.absX;
          tyDir = curLoc.absY - prevLoc.absY;
          if (xDir != txDir || yDir != tyDir) {
            xDir = txDir;
            yDir = tyDir;
            xOffsets.add(prevLoc.absX - me.loc.absX);
            yOffsets.add(prevLoc.absY - me.loc.absY);
          }
        }
        if (offset + 1 == tiles.length) {
          xOffsets.add(curLoc.absX - me.loc.absX);
          yOffsets.add(curLoc.absY - me.loc.absY);
          break;
        }
      }
      Path path = new Path(me.loc.absX, me.loc.absY, xOffsets, yOffsets);
      me.path = path;
      logger.finer('Path:\n$path');
      if (!me.pathHandler.finishedPath) {
        WalkTo walkTo = new WalkTo.fromPath(path);
        client.send(walkTo);
      }
    });
  }

  Point screenToPoint(num x, num y) => new Point<int>((x/SIZE).floor(), ((height-y)/SIZE).ceil());

  Point pointToScreen(int x, int y) => new Point<int>(x*SIZE, height-(y*SIZE));

  void update(num delta) {
    for (var player in players.values) {
      player.update(delta);
    }
    for (var mob in mobs.values) {
      mob.update(delta);
    }
  }

  void addEntity(Entity e) {
    if (e is Player) {
      if (!players.containsKey(e.def.name)) {
        players[e.def.name] = e;
        playerLayer.addChild(e);
        map.getTile(e.loc.absX, e.loc.absY).players.add(e);
        logger.fine("Player '${e.def.name}' was added to the region.");
      }
    } else if (e is Mob) {
      if (mobs.containsKey(e.id)) {
        removeEntity(e);
      }
      mobs[e.id] = e;
      mobLayer.addChild(e);
      map.getTile(e.loc.absX, e.loc.absY).mobs.add(e);
    } else if (e is GroundItem) {
      items.add(e);
      itemLayer.addChild(e);
      map.getTile(e.loc.absX, e.loc.absY).items.add(e);
    } else if (e is GroundObject) {
      if (!groundObjects.contains(e)) {
        groundObjects.add(e);
        if (map.getTile(e.loc.absX, e.loc.absY).object != null) {
          removeEntity(map.getTile(e.loc.absX, e.loc.absY).object);
        }
        map.getTile(e.loc.absX, e.loc.absY).object = e;
        objectLayer.addChild(e);
      }
    }
    e.location = e.loc;
  }

  void removeEntity(Entity e) {
    if (e is Player) {
      playerLayer.removeChild(e);
      players.remove(e.name);
      map.getTile(e.loc.absX, e.loc.absY).players.remove(e);
    } else if (e is Mob) {
      mobLayer.removeChild(e);
      mobs.remove(e.id);
      map.getTile(e.loc.absX, e.loc.absY).mobs.remove(e);
    } else if (e is GroundItem) {
      itemLayer.removeChild(e);
      items.remove(e);
      map.getTile(e.loc.absX, e.loc.absY).items.remove(e);
    } else if (e is GroundObject) {
      map.getTile(e.loc.absX, e.loc.absY).object = null;
      objectLayer.removeChild(e);
      groundObjects.remove(e);
    }
  }

  void setPosition(Entity e, Location loc, Point p) {
    if (loc != null) {
      map.getTile(loc.absX, loc.absY).remove(e);
    }
    if (p != null) {
      map.getTile(p.x, p.y).add(e);
    }
  }

  void render(RenderState renderState) {
    adjustScreenPosition();
    screen.hud.mouseOver.text = mouseOverText;

    super.render(renderState);
  }

  String get mouseOverText {
    Tile tile = map.getTile(localX, localY);
    if (tile != null) {
      String text = tile.topAction;
      if (text == '') {
        text = 'Walk-to (${localX}, ${localY})' + ((!tile.walkable && Client.DEBUG) ? ' - UNWALKABLE' : '');
      }
      return text;
    }
    return '';
  }

  void adjustScreenPosition() {
    // todo: could be an event or only update when the player moves? although with tweening maybe not
    x = screenX;
    y = screenY;
  }

  void cleanup() {
    // todo: implement method to speedup the cleanup of regions
  }
}
