part of adroit;

class Location {
  int absX;
  int absY;
  //num absZ;

  int region;

  Location.fromPosition(this.region, this.absX, this.absY) { }

  Location.fromPoint(this.region, Point p) {
    absX = p.x;
    absY = p.y;
  }

  String toString() {
    return '($absX, $absY)';
  }
}
