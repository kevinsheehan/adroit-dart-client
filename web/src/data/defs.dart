part of adroit;

class DefHandler {
  static final Logger logger = new Logger('DefHandler');

  static HashMap<int, GameObjDef> gameObjDefs = new HashMap();
  static HashMap<int, ItemDef> itemDefs = new HashMap();
  static HashMap<int, NpcDef> npcDefs = new HashMap();

  static Future load() {
    logger.info('Loading Definitions...');
    var futures = [];
    futures.add(html.HttpRequest.getString('defs/gameobjdefs.json')
      .then(loadGameObjDefs)
      .catchError(handleError));

    futures.add(html.HttpRequest.getString('defs/itemdefs.json')
      .then(loadItemDefs)
      .catchError(handleError));

    futures.add(html.HttpRequest.getString('defs/npcdefs.json')
      .then(loadNpcDefs)
      .catchError(handleError));

    return Future.wait(futures);
  }

  static void loadGameObjDefs(String json) {
    List<Map> jsonDefs = JSON.decode(json);
    for (Map jsonDef in jsonDefs) {
      GameObjDef def = new GameObjDef();
      def.id = jsonDef['objid'];
      def.name = jsonDef['name'];
      def.desc = jsonDef['description'];
      def.texture = jsonDef['texture'];
      def.action1 = jsonDef['action1'];
      def.action2 = jsonDef['action2'];
      def.walkable = jsonDef['walkable'];
      gameObjDefs[def.id] = def;
    }
    logger.info('Loaded ${gameObjDefs.length} GameObjDefs.');
  }

  static void loadItemDefs(String json) {
    List<Map> jsonDefs = JSON.decode(json);
    for (Map jsonDef in jsonDefs) {
      ItemDef def = new ItemDef();
      def.id = jsonDef['objid'];
      def.name = jsonDef['name'];
      def.desc = jsonDef['description'];
      def.texture = jsonDef['texture'];
      def.equipSlot = jsonDef['equipSlot'];
      def.reqAtk = jsonDef['reqAtk'];
      def.reqDef = jsonDef['reqDef'];
      def.stackable = jsonDef['stackable'];
      itemDefs[def.id] = def;
    }
    logger.info('Loaded ${itemDefs.length} ItemDefs.');
  }

  static void loadNpcDefs(String json) {
    List<Map> jsonDefs = JSON.decode(json);
    for (Map jsonDef in jsonDefs) {
      NpcDef def = new NpcDef();
      def.id = jsonDef['npcid'];
      def.name = jsonDef['name'];
      def.desc = jsonDef['description'];
      def.texture = jsonDef['texture'];
      def.action = jsonDef['action'];
      def.level = jsonDef['level'];
      def.attackable = jsonDef['attackable'];
      def.aggressive = jsonDef['aggressive'];
      def.health = jsonDef['health'];
      def.str = jsonDef['str'];
      def.spd = jsonDef['spd'];
      def.def = jsonDef['def'];
      npcDefs[def.id] = def;
    }
    logger.info('Loaded ${npcDefs.length} NpcDefs.');
  }

  static void handleError(Error e) {
    logger.severe('Error while trying to load definitions', e);
  }
}

class EntityDef {
  int id;
  String name;
  String desc;
  String texture;
}

class GameObjDef extends EntityDef {
  String action1;
  String action2;
  bool walkable;
}

class ItemDef extends EntityDef {
  int equipSlot;
  int reqAtk;
  int reqDef;
  bool stackable;
}

class NpcDef extends EntityDef {
  String action;
  int level;
  bool attackable;
  bool aggressive;
  int health;
  int str;
  int spd;
  int def;
}
