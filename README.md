Adroit Dart Client
==================
This is the web client being developed for [Adroit](http://ksdev.org/projects/adroit "KSDev Adroit Project"), the MMORPG game, created by Kevin Sheehan.

### Status ###
This client is no longer under development. Dart, in it's current state, is not an appropriate solution for a web Game Client. It is too bad, I actually enjoyed developing this very much. The Java feel of a language with a target platform of HTML5 was quite enjoyable. Feel free to browse around and use any of it for yourself.

### Server ###
I am not open sourcing the Server code. This code is still very relevant to personal projects of mine and is something that I will continue to work on.

### Basic Design ###
The design of the Client/Server interactions is quite simple. Being a web based game, it uses WebSockets for communication and messages written in regular JSON.

All relevant definition data the client needs to know for the different in game entities are stored and loaded from JSON files.